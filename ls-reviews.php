<?php
/*
Plugin Name: Reviews
Plugin URI: https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/
Description: Use a shortcode to create a form which directs users to a good / bad review page.
Version: 1.0.2
Author: Sam Fullalove
Author URI: http://sam.fullalove.co/
Text Domain: ls-reviews
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
USA
*/

/**
* run this code when plugin is activated
*/
function ls_reviews_activation() {
    global $wp_version;

    //make sure running minimum wordpress version required to use wp functions (just use dev version)
    if ( version_compare( $wp_version, '4.1', '<' ) ) {
        wp_die( 'This plugin requires WordPress version 4.1 or higher.' );
    }
}
register_activation_hook( __FILE__, 'ls_reviews_activation' );

/**
 * Enqueue css
 */
function ls_reviews_css() {
    wp_register_style( 'ls_reviews_css', plugin_dir_url( __FILE__ ) . 'css/style.css', false, '1.0.0' );
    wp_enqueue_style( 'ls_reviews_css' );
}
add_action( 'wp_enqueue_scripts', 'ls_reviews_css', 11 );

/**
* Add a menu item in the admin area sidebar
**/
function ls_reviews_menu(){


/*if we ever want to change it to a main page...
  add_menu_page(
      'Review Settings', // ID of menu with which to register this submenu
      __('Review Settings', 'ls-reviews'), // the text for this item
      'manage_options', // which type of users can see this menu (Make this manage_network for super users only)
      'ls_reviews_settings', // unique ID (the slug) for this menu item
      'ls_reviews_settings_page', // callback function
      'dashicons-star-filled' //icon
    ); */

    add_submenu_page(
    'options-general.php',
    __('Review Settings', 'ls-reviews'), // the page title text
    __('Review Settings', 'ls-reviews'), // the menu text
    'manage_options', // which type of users can see this menu (Make this manage_network for super users only)
    'ls_reviews_settings', // unique ID (the slug) for this menu item
    'ls_reviews_settings_page' // callback function
    );


}
add_action('admin_menu', 'ls_reviews_menu');

/**
* Displays and saves the business details page
**/
function ls_reviews_settings_page() {
    //list of variables we are using:
    $varList = array(
        'stars',
        'goodPage',
        'badPage'
    );

    //update the database if the form is submitted
    if(isset($_POST['update']))
    {
        //only save the post variables we allow, this stop users adding more options, but is kinda hardcoded which sucks...
        foreach ($varList as $var ) {
          $toSave[$var] = sanitize_text_field($_POST[$var]);
        }

        //save to the database
        set_transient('ls-reviews', $toSave, MONTH_IN_SECONDS);
        update_option('ls-reviews', $toSave);

        //display the update success box
        ?>
            <div class="notice notice-success is-dismissible">
              <p>Your settings have been successfully saved.</p>
              <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
            </div>
        <?php
    }

    //get the options from the transient
    $options = get_transient( 'ls-reviews' );

    //if the transient expired then use the options
    if(empty($options)){
        $options = get_option('ls-reviews');
    }

    //set the defaults as blank/ init the variables for the form to display
    foreach ($varList as $var ) {

        if(!isset($options[$var]) && $var == 'stars' ){
            $options['stars'] = 3;
        }

        if(!isset($options[$var]))
        {
            $options[$var] = "";
        }

    }

    //display the form
    include_once(dirname(__FILE__) . '/settings-form.php');

}

//Shortcode for displaying form
function ls_reviews_form_shortcode($atts){

    $a = shortcode_atts( array(
    'button' => 'Rate Us',
    ), $atts );

    return '
        <form class="ls-reviews-form" id="" action="'.get_site_url().'/wp-content/plugins/ls-reviews/redirect.php" method="post">
              <input type="hidden" name="update" value="ls-reviews-form">

              <span class="ls-star-rating">
                  <input type="radio" id="star1" name="stars" value="1" required/><i></i>
                  <input type="radio" id="star2" name="stars" value="2" required/><i></i>
                  <input type="radio" id="star3" name="stars" value="3" required/><i></i>
                  <input type="radio" id="star4" name="stars" value="4" required/><i></i>
                  <input type="radio" id="star5" name="stars" value="5" required/><i></i>
              </span>

              <br>

              <input class="ls-reviews-stars-button" type="submit" value="'.$a['button'].'">
        </form>
    ';

}
add_shortcode( 'ls_review_form', 'ls_reviews_form_shortcode' );

?>
