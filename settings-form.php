<?php
?>

<div class="wrap bus_detail_input">
  <h1>Review Page Settings</h1>

  <div class="clear"></div>
  <p style="font-size:14px;">This plugin will help you send users to 2 different pages when they review your business. Users that rate the same or higher than your set stars will be directed to your good page. Users that rate less than this will go to the bad page. To use this to your advantage, we recommend putting helpful review links on your good page and a contact form on your bad page.</p>
  <p style="font-size:14px;">To use this, add the shortcode [ls_review_form] on a frontend page</p>
  <form id="" action="" method="post">
        <input type="hidden" name="update" value="ls-reviews-update">
        <table class="form-table">
            <tbody>

              <tr>
                  <th scope="row"><label for="stars">Number of stars</label></th>
                  <td>
                      <input id="stars" type="number" min="1" max="5" name="stars" value="<?php echo stripslashes($options['stars']); ?>">
                  </td>
              </tr>

              <tr>
                  <th scope="row"><label for="goodPage">Good review page</label></th>
                  <td>
                      <?php
                      $dropdown_args = array(
                        'name'             => 'goodPage',
                        'show_option_none' => __('I haven\'t made my page yet'),
                        'selected'         => $options['goodPage'],
                        'echo'             => 1,
                      );

                      wp_dropdown_pages( $dropdown_args );
                      ?>
                  </td>
              </tr>

              <tr>
                  <th scope="row"><label for="badPage">Bad review page</label></th>
                  <td>
                      <?php
                          $dropdown_args = array(
                            'name'             => 'badPage',
                            'show_option_none' => __('I haven\'t made my page yet'),
                            'selected'         => $options['badPage'],
                            'echo'             => 1,
                          );

                          wp_dropdown_pages( $dropdown_args );
                      ?>
                  </td>
              </tr>

            </tbody>
        </table>

        <p>
            <input type="submit" class="button button-primary" value="<?php _e('Save Changes', 'ls-reviews'); ?>">
        </p>

  </form>

</div>
