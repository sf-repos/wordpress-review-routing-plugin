<?php
/*THIS FILE HANDLES THE SHORTCODE FORM, IT REDIRECTS USERS BASED ON THEIR RATING*/

//This is a standalone php file, so we must load the wp functions first
require ('../../../wp-load.php');

//This redirects users based on the shortcode form
if(isset($_POST['update']))
{
    //get the options from the transient
    $options = get_transient( 'ls-reviews' );

    //if the transient expired then use the options
    if(empty($options)){
        $options = get_option( 'ls-reviews' );

        //if we are here, then we need to set the transient back up
        set_transient('ls-reviews', $options, MONTH_IN_SECONDS);
    }

    //display an error if the pages are not set
    if(empty($options['goodPage']) || empty($options['badPage'])){
        echo "ERROR: Please set a Good Page and Bad Page in the <a href=".get_site_url()."/wp-admin/options-general.php?page=ls_reviews_settings>settings</a>";
        exit;
    }

    //redirect to the good or bad page based on the stars submitted
    if ( $_POST['stars'] >= $options['stars'] )
    {
        wp_safe_redirect( get_permalink( $options['goodPage'] ) );
        exit;
    } else {
        wp_safe_redirect( get_permalink( $options['badPage'] ) );
        exit;
    }

}
