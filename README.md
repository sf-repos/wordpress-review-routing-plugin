# ls-reviews

This plugin will help you send users to 2 different pages when they review your business. Users that rate the same or higher than your set stars will be directed to your good page. Users that rate less than this will go to the bad page. To use this to your advantage, we recommend putting helpful review links on your good page and a contact form on your bad page.

To use this:
1: make 3 pages, 1 for the review star shortcode, 1 good landing page and 1 bad landing page 
2: go to settings->review settings and set the number of stars, the good and bad page.
3: add the shortcode [ls_review_form] on the page you made for it.

How the shortcode looks (button follows your theme colors):

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/review.png)

2 star:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/2.png)

3 star:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/3.png)

4 star:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/4.png)

5 star:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/5.png)

Quick example of bad page:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/bad.png)

Quick example of good page:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/good.png)

Settings page:

![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/settings.png)


![picture](https://bitbucket.org/sf-repos/wordpress-review-routing-plugin/raw/master/screenshots/shortcode.png)